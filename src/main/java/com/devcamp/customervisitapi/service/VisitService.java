package com.devcamp.customervisitapi.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.customervisitapi.model.Visit;

@Service
public class VisitService {
    @Autowired
    private CustomerService customerService;  // chỉ cáp dụng trong phương thức
    //Bộ dữ liệu bao gồm tối thiểu 3 chuyến đi. Mỗi chuyến đi tương ứng với 1 customer.
    
    // phương thức trả về ArrayList tất cả các chuyến đi
    public ArrayList <Visit> getAllVisit(){
        ArrayList <Visit>  visitAll = new ArrayList<Visit>();
        Visit chuyen_di_1 = new Visit(customerService.customer1, new Date());
        Visit chuyen_di_2 = new Visit(customerService.customer2, new Date());
        Visit chuyen_di_3 = new Visit(customerService.customer3, new Date());

        visitAll.add(chuyen_di_1);
        visitAll.add(chuyen_di_2);
        visitAll.add(chuyen_di_3);

        return visitAll;
    }
}
