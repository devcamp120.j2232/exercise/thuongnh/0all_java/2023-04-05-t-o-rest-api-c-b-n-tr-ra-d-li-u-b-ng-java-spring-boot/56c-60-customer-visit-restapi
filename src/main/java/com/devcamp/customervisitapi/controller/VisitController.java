package com.devcamp.customervisitapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customervisitapi.model.Visit;
import com.devcamp.customervisitapi.service.VisitService;

import org.springframework.web.bind.annotation.GetMapping;


@RestController
@CrossOrigin

public class VisitController {

    @Autowired
    private VisitService visitService;;

    @GetMapping(value="visits")
    public ArrayList<Visit> getMethodName() {
        return visitService.getAllVisit();
    }
    
    
}
